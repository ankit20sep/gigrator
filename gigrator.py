# -*- coding: utf-8 -*-

"""
@author: Ankit <ankit2012jan@gmail.com>
@date: 2021/1/27

"""
import json
import logging
import os
import re
import uuid
from urllib.parse import quote
import requests
import settings
import shutil

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class Git:
    type = ''
    username = ''
    token = ''
    url = ''
    api = ''
    ssh_prefix = ''
    headers = {}

    def __init__(self, config: dict):
        self.type = config['type'].lower()
        if self.type == 'gogs':
            self.type = 'gitea'
        self.username = config['username']
        self.token = config['token']
        self.url = config['url']

        if not self.type:
            raise ValueError( 'type must be configured')

        if self.type not in settings.SUPPORT_GITS:
            raise ValueError('This type of Git server is not currently supported: ' + self.type)

        if self.type in ['gitlab', 'gitea'] and not self.url:
            raise ValueError('gitlab/gitea/gogs needs to configure url')

        if not self.username:
            raise ValueError('username must be configured')

        if not self.token:
            raise ValueError('token must be configured')

        if self.type in ['gitlab', 'gitea']:
            if not re.match(r'^http(s)?://.+$', self.url):
                raise ValueError('url configuration error')
            if self.url.endswith('/'):
                self.url = self.url[:-1]

    def is_existed(self, repo_name: str) -> bool:
        """
        Check repo existed or not

        :param repo_name:
        :return:
        """
        raise NotImplementedError

    def rmdir(directory):
        directory = Path(directory)
        for item in directory.iterdir():
            if item.is_dir():
                rmdir(item)
            else:
                item.unlink()
        directory.rmdir()

    def clone_repo(self, repo_name: str) -> str:
        """
        Clone repo

        :param repo_name:
        :return: the local dir of saved repo
        """

        clone_space = os.path.join(settings.TEMP_DIR, "")
        if not os.path.exists(clone_space):
            os.mkdir(clone_space)

        # Check whether there is a repo locally, delete if it exists
        ssh_address = self.ssh_prefix + self.username + '/' + repo_name + '.git'
        clone_cmd = 'cd ' + clone_space + ' && git clone --bare ' + ssh_address
        print('cloning : '+clone_cmd)
        print('Checking : '+clone_space+repo_name + '.git')
        if os.path.exists(clone_space+repo_name + '.git'):
            shutil.rmtree(clone_space+repo_name + '.git')
        os.system(clone_cmd)
        return os.path.join(clone_space, repo_name + '.git')

    def create_repo(self, name: str, desc: str, is_private: bool) -> bool:
        """
        Create repo

        :param name:
        :param desc:
        :param is_private:
        :return: create successfully or not
        """
        raise NotImplementedError

    def fetch_repo(self, repo_name: str) -> bool:
        """
        Create repo

        :param name:
        :param desc:
        :param is_private:
        :return: create successfully or not
        """
        print('Inside fetch_repo ')
        clone_space = os.path.join(settings.TEMP_DIR, repo_name+".git")
        fetch_cmd = 'cd ' + clone_space + ' && git fetch --all '
        print('fetch_cmd:' + fetch_cmd)
        return os.system(fetch_cmd) == 0

    def push_repo(self, repo_name: str, repo_dir: str) -> bool:
        """
        Push repo

        :param repo_name:
        :param repo_dir:
        :return: bool
        """
        print('Inside push_repo ')
        ssh_address = self.ssh_prefix + self.username + '/' + repo_name + '.git'
        push_cmd = 'cd ' + repo_dir + ' && git push ' + ssh_address + ' --mirror'
        print('push_cmd:'+push_cmd)
        return os.system(push_cmd) == 0

    def list_repos(self) -> list:
        """
        List all repos owned

        :return: a list of repos
        """
        raise NotImplementedError


class Gitlab(Git):

    def __init__(self, config: dict):
        super().__init__(config)
        self.headers = {
            'Private-Token': self.token
        }
        self.ssh_prefix = 'git@' + self.url.split('://')[1] + ':'
        self.api = self.url + settings.GITLAB_API_VERSION

    def is_existed(self, repo_name: str) -> bool:
        # Get single project
        # GET /projects/:id
        path = quote(f'{self.username}/{repo_name}', safe='')
        url = f'{self.api}/projects/{path}'
        r = requests.get(url, headers=self.headers)
        return r.status_code == 200

    def create_repo(self, name: str, desc: str, is_private: bool) -> bool:
        print('inside create_repo ')
        data = {
            'name': name,
            'description': desc,
            'visibility': 'private' if is_private else 'public'
        }
        url = f'{self.api}/projects'
        print('inside create_repo --url'+url)
        r = requests.post(url, data=data, headers=self.headers)
        print(r.content)
        return r.status_code == 201 or r.status_code == 404

    def list_repos(self) -> list:
        # GitLab
        # List user projects: GET /users/:user_id/projects (Pagination required: ?page=1)
        # No 401 problem exists. The public repository is returned.
        url = f'{self.api}/users/{self.username}/projects?page='
        # If no authorization is granted, only public projects may be returned (at least GitLab will be returned).
        all_repos = []
        page = 1
        while True:
            r = requests.get(url + str(page), headers=self.headers)
            if r.status_code == 200:
                repos = r.json()
                if len(repos) == 0:
                    break
                for repo in repos:
                    all_repos.append(dict(name=repo['name'],
                                          desc=repo['description'],
                                          is_private=repo['visibility'] != 'public'))
                page += 1
            else:
                raise RuntimeError(r.content)
        return all_repos


class Github(Git):
    def __init__(self, config: dict):
        super().__init__(config)
        self.headers = {
            'Authorization': 'token ' + self.token
        }
        self.ssh_prefix = settings.GITHUB_SSH_PREFIX
        self.api = settings.GITHUB_API

    def is_existed(self, repo_name: str) -> bool:
        query = '''
        query ($repo_owner: String!, $repo_name: String!) {
          repository(owner: $repo_owner, name: $repo_name) {
            id
          }
        }
        '''
        variables = {
            'repo_owner': self.username,
            'repo_name': repo_name
        }
        post_data = json.dumps({
            'query': query,
            'variables': variables
        })
        r = requests.post(self.api, data=post_data, headers=self.headers)
        if r.status_code == 200:
            data = r.json()
            try:
                return data['data']['repository'].get('id', None) is not None
            except KeyError:
                return False
        else:
            raise RuntimeError(r.content)

    def create_repo(self, name: str, desc: str, is_private: bool) -> bool:

        print('inside...create_repo')
        mutation = '''
        mutation ($name: String!, $desc: String!, $isPrivate: RepositoryVisibility!) {
          createRepository(input: {name: $name, description: $desc, visibility: $isPrivate}) {
            clientMutationId
            repository {
              id
            }
          }
        }
        '''
        variables = {
            'name': name,
            'desc': desc,
            'isPrivate': 'PRIVATE' if is_private else 'PUBLIC'
        }
        post_data = json.dumps({
            'query': mutation,
            'variables': variables
        })
        print('create_repo API:'+self.api + 'post:'+post_data)
        r = requests.post(self.api, data=post_data, headers=self.headers)
        if r.status_code == 200:
            data = r.json()
            print('response:'+data)
            return 'errors' not in data.keys()
        else:
            raise RuntimeError(r.content)

    def list_repos(self) -> list:
        query = '''
        query ($first: Int!, $after: String) {
          viewer {
            repositories(first: $first, after: $after, ownerAffiliations: [OWNER]) {
              edges {
                node {
                  name
                  isPrivate
                  description
                }
                cursor
              }
              pageInfo {
                hasNextPage
              }
            }
          }
        }
        '''
        variables = {
            'first': 100
        }
        post_data = json.dumps({
            'query': query,
            'variables': variables
        })
        r = requests.post(self.api, data=post_data, headers=self.headers)
        if r.status_code == 200:
            data = r.json()
            all_repos = []
            try:
                def parse_data():
                    repos = data['data']['viewer']['repositories']['edges']
                    for repo in repos:
                        repo = repo['node']
                        all_repos.append(dict(name=repo['name'],
                                              desc=repo['description'],
                                              is_private=repo['isPrivate']))
                parse_data()
                has_next_page = data['data']['viewer']['repositories']['pageInfo']['hasNextPage']
                while has_next_page:
                    variables['after'] = data['data']['viewer']['repositories']['edges'][-1]['cursor']
                    post_data = json.dumps({
                        'query': query,
                        'variables': variables
                    })
                    r = requests.post(self.api, data=post_data, headers=self.headers)
                    if r.status_code == 200:
                        data = r.json()
                        parse_data()
                        has_next_page = data['data']['viewer']['repositories']['pageInfo']['hasNextPage']
                    else:
                        raise RuntimeError(r.content)
            except KeyError:
                logger.error(data)
            finally:
                return all_repos
        else:
            raise RuntimeError(r.content)


class Gitee(Git):
    def __init__(self, config: dict):
        super().__init__(config)
        self.headers = {
            'Content-Type': 'application/json;charset=UTF-8'
        }
        self.ssh_prefix = settings.GITEE_SSH_PREFIX
        self.api = settings.GITEE_API

    def is_existed(self, repo_name: str) -> bool:
        # Get a user's warehouse: GET /repos/{owner}/{repo}
        # https://gitee.com/api/v5/swagger#/getV5ReposOwnerRepo
        url = f'{self.api}/repos/{self.username}/{repo_name}?access_token={self.token}'
        r = requests.get(url, headers=self.headers)
        return r.status_code == 200

    def create_repo(self, name: str, desc: str, is_private: bool) -> bool:
        data = {
            'access_token': self.token,
            'name': name,
            'description': desc,
            'private': is_private
        }
        url = f'{self.api}/user/repos'
        print('create_repo url:' + url)
        r = requests.post(url, json=data, headers=self.headers)
        return r.status_code == 201

    def list_repos(self) -> list:
        # Gitee
        # List all repositories of authorized users: GET /user/repos
        # https://gitee.com/api/v5/swagger#/getV5UserRepos
        list_repos_url = self.api + '/user/repos?access_token=' + self.token \
                         + '&type=personal&sort=full_name&per_page=100&page='
        page = 1
        all_repos = []
        while True:
            r = requests.get(list_repos_url + str(page), headers=self.headers)
            if r.status_code == 200:
                repos = r.json()
                if len(repos) == 0:
                    break
                for repo in repos:
                    all_repos.append(dict(name=repo['name'],
                                          desc=repo['description'],
                                          is_private=repo['private']))
                page += 1
            elif r.status_code == 401:
                raise ValueError('token 无效')
            else:
                raise RuntimeError(r.content.decode('utf-8'))
        return all_repos


class Gitea(Git):
    def __init__(self, config: dict):
        super().__init__(config)
        self.headers = {
            'Content-Type': 'application/json',
            'Authorization': f'token {self.token}'
        }
        self.ssh_prefix = f'git@{self.url.split("://")[1]}:'
        self.api = self.url + settings.GITEA_API_VERSION

    def is_existed(self, repo_name: str) -> bool:
        # GET
        # ​/repos​/{owner}​/{repo}
        # Get a repository
        url = f'{self.api}/repos/{self.username}/{repo_name}'
        r = requests.get(url, headers=self.headers)
        return r.status_code == 200

    def create_repo(self, name: str, desc: str, is_private: bool) -> bool:
        data = {
            'auto_init': False,
            'description': desc,
            'name': name,
            'private': is_private
        }
        url = f'{self.api}/user/repos'
        print('create_repo: url:'+url)
        r = requests.post(url, headers=self.headers, data=data)
        return r.status_code == 201

    def list_repos(self) -> list:
        # GET
        # ​/user​/repos
        # List the repos that the authenticated user owns or has access to
        # No pagination: https://github.com/go-gitea/gitea/issues/7515
        list_repos_url = self.api + '/user/repos'
        all_repos = []
        r = requests.get(list_repos_url, headers=self.headers)
        if r.status_code == 200:
            repos = r.json()
            for repo in repos:
                if repo['owner']['username'] == self.username:
                    all_repos.append(dict(name=repo['name'],
                                          desc=repo['description'],
                                          is_private=repo['private']))
        return all_repos


class Coding(Git):
    def __init__(self, config: dict):
        super().__init__(config)
        self.headers = {
            'Authorization': 'token ' + self.token
        }
        self.ssh_prefix = settings.CODING_SSH_PREFIX
        self.api = f'https://{self.username}.coding.net'

    def is_existed(self, repo_name: str) -> bool:
        # GET /api/user/{username}/project/{project_name}
        url = f'{self.api}/api/user/{self.username}/project/{repo_name}'
        r = requests.get(url, headers=self.headers)
        if r.status_code == 200:
            data = r.json()
            return data['code'] == 0 and data['data']['name'] == repo_name
        else:
            return False

    def create_repo(self, name: str, desc: str, is_private: bool) -> bool:
        raise PermissionError('Coding does not support creating warehouses through API')

    def list_repos(self) -> list:
        # Current user's project list
        # https://open.coding.net/api-reference/%E9%A1%B9%E7%9B%AE.html#%E5%BD%93%E5%89%8D%E7%94%A8%E6%88%B7%E7%9A%84%E9%A1%B9%E7%9B%AE%E5%88%97%E8%A1%A8
        # GET /api/user/projects?type=all&amp;page={page}&amp;pageSize={pageSize}
        # Response包含totalPage
        url = f'{self.api}/api/user/projects?type=all&pageSize=10&page='
        page = 1
        all_repos = []
        r = requests.get(url + str(page), headers=self.headers)
        if r.status_code == 200:
            data = r.json()
            if data['code'] == 0:
                total_page = data['data']['totalPage']
                repos = data['data']['list']
                for repo in repos:
                    if str.lower(repo['owner_user_name']) == str.lower(self.username):
                        all_repos.append(dict(name=repo['name'],
                                              desc=repo['description'],
                                              is_private=not repo['is_public']))
            else:
                raise RuntimeError(data)

            while page < total_page:
                page += 1
                r = requests.get(url + str(page), headers=self.headers)
                if r.status_code == 200:
                    if data['code'] == 0:
                        data = r.json()
                        repos = data['data']['list']
                        for repo in repos:
                            if str.lower(repo['owner_user_name']) == str.lower(self.username):
                                all_repos.append(dict(name=repo['name'],
                                                      desc=repo['description'],
                                                      is_private=not repo['is_public']))
                    else:
                        raise RuntimeError(data)

        return all_repos


if __name__ == "__main__":
    if not os.path.isdir(settings.TEMP_DIR):
        os.mkdir(settings.TEMP_DIR)

    source_type = settings.SOURCE_GIT.get('type', '')
    dest_type = settings.DEST_GIT.get('type', '')
    if source_type == 'gitlab':
        source_git = Gitlab(settings.SOURCE_GIT)
    elif source_type == 'github':
        source_git = Github(settings.SOURCE_GIT)
    elif source_type == 'coding':
        source_git = Coding(settings.SOURCE_GIT)
    elif source_type in ['gitea', 'gogs']:
        source_git = Gitea(settings.SOURCE_GIT)
    elif source_type == 'gitee':
        source_git = Gitee(settings.SOURCE_GIT)
    else:
        raise ValueError(f'This type of Git server is not currently supported: {source_type}')

    if dest_type == 'gitlab':
        dest_git = Gitlab(settings.DEST_GIT)
    elif dest_type == 'github':
        dest_git = Github(settings.DEST_GIT)
    elif dest_type == 'coding':
        raise ValueError(f'Does not support migration to Coding')
    elif dest_type in ['gitea', 'gogs']:
        dest_git = Gitea(settings.DEST_GIT)
    elif dest_type == 'gitee':
        dest_git = Gitee(settings.DEST_GIT)
    else:
        raise ValueError(f'This type of Git server is not currently supported: {source_type}')

    all_repos = source_git.list_repos()
    for i, repo in enumerate(all_repos):
        print(f'{str(i)}. {repo["name"]}')
    # repo_ids = [int(i) for i in input('Please enter the serial number of the warehouse to be migrated, separated by commas: ').replace(' ', '').split(',')]
    for i, repo in enumerate(all_repos):
        migrate_repo = all_repos[i]
        try:
            print('Migrating : '+migrate_repo['name'])
            repo_dir = source_git.clone_repo(migrate_repo['name'])
            print('Clone completed :'+repo_dir)
            if repo_dir:
                print('repo creation started')
                has_create = dest_git.create_repo(**migrate_repo)
                print("Repo created", 'is', bool(has_create))
                has_create = dest_git.fetch_repo(migrate_repo['name'])
                print("Repo Fetch", 'is', bool(has_create))
                print('Repo created:has_create:')
                if has_create:
                    print('push_repo starting....')
                    dest_git.push_repo(migrate_repo['name'], repo_dir)
                    print('push_repo completing....')
        except Exception as e:
            logger.error(e)

