# GitHub configuration
GITHUB = {
    'type': 'github',
    'username': 'your_username',
    'token': 'your_token',
    'url' :  ''   # Don't need to fill in
}

# Gitee
GITEE  =  {
    'type': 'gitee',
    'username': 'your_username',
    'token': 'your_token',
    'url' :  ''   # Don't need to fill in
}

# GitLab
GITLAB  =  {
    'type': 'gitlab',
    'username': 'your_username',
    'token': 'your_token',
    'url' :  'https   : //git.your_domain.com' # Git server address
}

# Gitea / Gogs,
GITEA_OR_GOGS = {
    'type' :  'gitea' ,   # Both Gitea and Gogs fill in gitea
    'username': 'your_username',
    'token': 'your_token',
    'url' :  'https   : //git.your_domain.com' # Git server address
}

# Coding
CODING = {
    'type' :  'coding' ,   # Both Gitea and Gogs fill in gitea
    'username' :  'your_username' ,   # username is the secondary domain name of the Coding address, for example, my Coding address is https://hsowan.coding.net, hsowan is my username
    'token': 'your_token',
    'url' :  'https   : //git.your_domain.com' # Git server address
}