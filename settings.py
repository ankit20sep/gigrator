# -*- coding: utf-8 -*-


import os

# type Git server type, for example: gitee, github, gitlab, gitea, gogs, coding, required
# username is the username of the Git server, required
# token The authorization token of the Git server where the user is located, required
# url Git server access address, for example: https://git.example.com (including specific protocol: http/https)
# The Git servers that need to set the url are: gitlab, gitea, gogs, other Git servers are empty by default
# Source Git server configuration
SOURCE_GIT = {
    'type': 'gitee',
    'username': 'ankit20sep',
    'token': '7455216c7484a133d0bc9b2de2d90d6b',
    'url': 'https://gitee.com/ankit20sep'
}
# Destination Git server configuration
DEST_GIT = {
    'type': 'gitlab',
    'username': 'ankit20sep',
    'token': 'zySW8wBarqnqU_Qu2ZyT',
    'url': 'https://gitlab.com'
}


# Supported Git server
SUPPORT_GITS = ['gitlab', 'github', 'gitee', 'gitea', 'coding', 'gogs']

# Warehouse temporary storage directory
TEMP_DIR = os.path.join(os.path.dirname(__file__), '.repos')

# GitLab
GITLAB_API_VERSION = '/api/v4'

# GitHub
# GitHub Enterprise is not currently supported
GITHUB_API = 'https://api.github.com/graphql'
GITHUB_SSH_PREFIX = 'git@github.com:'

# Code Cloud
GITEE_API = 'https://gitee.com/api/v5'
GITEE_SSH_PREFIX = 'git@gitee.com:'

# Coding
# Private deployment is not currently supported
CODING_SSH_PREFIX = 'git@e.coding.net:'

# Gitea/Gogs
GITEA_API_VERSION = '/api/v1'
